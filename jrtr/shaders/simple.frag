#version 150
// GLSL version 1.50
// Fragment shader for diffuse shading in combination with a texture map

// Uniform variables passed in from host program
uniform sampler2D myTexture;

// Variables passed in from the vertex shader
in vec2 frag_texcoord;

// Output variable, will be written to framebuffer automatically
out vec4 out_color;

void main()
{		
	// The built-in GLSL function "texture" performs the texture lookup
	out_color = texture(myTexture, frag_texcoord);
}

